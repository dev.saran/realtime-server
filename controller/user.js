const Users = require("../models/userModel");
const valid = require("../utils/validation");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

async function registeruser(req, res) {
  try {
    const { name, email, password, cf_password } = req.body;
    const errMessage = valid(name, email, password, cf_password);
    if (errMessage) return res.status(400).send({ msg: errMessage });

    const checkUser = await Users.findOne({ email });

    if (checkUser)
      return res.status(409).send({ msg: "User already exists. Please Login" });
    const passwordHash = await bcrypt.hash(password, 12);
    const user = new Users({
      name,
      email: email.toLowerCase(),
      type: 'user',
      password: passwordHash,
      cf_password,
      type: "user"
    });
    // Create token
    const token = jwt.sign(
      { user_id: user._id, email },
      process.env.SECRET_KEY,
      {
        expiresIn: "2h",
      }
    );
    // save user token
    user.token = token;
    await user.save();
    res.status(200).send({ msg: "Successfully Registered, Please Login now" });
  } catch (error) {
    res.status(400).send({ msg: "Something went wrong" });
  }
}

module.exports = registeruser;
