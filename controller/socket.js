let users = [];
module.exports = (io, socket) => {
  const disconnect = (payload) => {
    //Updates the list of users when a user disconnects from the server
    users = users.filter((user) => user.socketID !== socket.id);
    // console.log(users);
    //Sends the list of users to the client
    io.emit("newUserResponse", users);
    socket.disconnect();
  };
  const messageHandler = (data) => {
    io.emit("message:flow", data);
  };
  const handleNewUser = (data) => {
    //Adds the new user to the list of users
    users.push(data);
    // console.log(users);
    //Sends the list of users to the client
    io.emit("user:response", users);
  };

  socket.on("user:disconnect", disconnect);
  socket.on("user:message", messageHandler);
  socket.on("user:new", messageHandler);
  //Listens when a new user joins the server
  socket.on("newUser", handleNewUser);

  socket.emit("user:connect", socket.id);

  socket.on("disconnect", () => {
    socket.broadcast.emit("callEnded");
  });

  socket.on("callUser", ({ userToCall, signalData, from, name }) => {
    io.to(userToCall).emit("callUser", { signal: signalData, from, name });
  });

  socket.on("answerCall", (data) => {
    io.to(data.to).emit("callAccepted", data.signal);
  });
};
