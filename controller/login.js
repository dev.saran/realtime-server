const Users = require("../models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const authenticateuser = async (req, res) => {
  const { email, password, isGoogle, image, _id, username } = req.body;
  if (isGoogle) {
    const token = await jwt.sign(
      { id: _id, email },
      process.env.SECRET_KEY,
      {
        expiresIn: "2h",
      }
    );
    res.status(200).send({
      msg: "logged in",
      data: { isAuthorised: true, token: token, username: email },
    });
  } else {
    if (!(email && password)) {
      res.status(422).send({ msg: "All Fields are required" });
    }
    const checkUser = await Users.findOne({ email });
    if (!checkUser) {
      return res.status(401).send({ msg: "User doesn't exists" });
    } else {
      const isMatch = await bcrypt.compare(password, checkUser.password);
      if (!isMatch) return res.status(422).json({ err: "Incorrect password" });
      const token = await jwt.sign(
        { id: checkUser._id, email },
        process.env.SECRET_KEY,
        {
          expiresIn: "2h",
        }
      );
      checkUser.token = token;
      await checkUser.save();
      res.status(200).send({
        msg: "logged in",
        data: { isAuthorised: true, token: token, username: checkUser.name, email: checkUser.email, type: checkUser.type },
      });
    }
  }
};

module.exports = authenticateuser;
