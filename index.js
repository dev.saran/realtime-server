const app = require("express")();
const server = require("http").createServer(app);
const cors = require("cors");
const handleSocketController = require("./controller/socket");
require("dotenv").config({ path: ".env" });

const io = require("socket.io")(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"],
	},
});

app.use(cors());


const PORT = process.env.PORT || 5000;

app.get("/", (req, res) => {
	res.send("Server is running");
});

io.on("connection", (socket) => {
    handleSocketController(io, socket)
});

server.listen(PORT, () => {
	console.log(`Server listening on port ${PORT}`);
});
